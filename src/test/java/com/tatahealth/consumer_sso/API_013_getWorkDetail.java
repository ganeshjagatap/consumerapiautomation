package com.tatahealth.consumer_sso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import core.CommonData;
import core.Constants;
import core.Envirnonment;
import core.RequestUtil;
import core.TestBase;

public class API_013_getWorkDetail extends TestBase
{

	
	public static JSONObject responseJson_object=null;
	public static StringBuffer sb=null;
	
	
	public static String mailUrl=null;
	//String token=null;
	public String date1=null;
	static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static Date date = new Date();
	@Test(priority=1)
	public void API_013_getWorkDetail_SuccessUsecase() throws Exception{
	    
		int consumerUserId= CommonData.getconsumerUserId();
		String authToken= CommonData.getauthToken();
		mailUrl = Envirnonment.env + Constants.getWorkDetail;
		System.out.println("mailUrl "+mailUrl);
		//String RequstUrl = Envirnonment.env + Constants.CREATE_SESSION_PATH;
		RequestUtil requestUtil = new RequestUtil();

		// Get request for the session creation first.
		//HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		System.out.println("begin");
	
		List<NameValuePair> login = Consumer_SSO_Param_Generator.getgetWorkDetailParameters(consumerUserId);
		
		// Post request to the URL with the param data
		HttpResponse httpResponseForResetPassword = requestUtil.postJSON_Request(mailUrl, login, authToken);
		responseJson_object = requestUtil.getJSONObjectForResponse(httpResponseForResetPassword);
		int successFlag=responseJson_object.getJSONObject("status").getInt("httpStatusCode");
		Assert.assertEquals(successFlag,200, "Some error occured. "+ responseJson_object.getJSONObject("status").getString("message"));
		
		
	
	}	
}
