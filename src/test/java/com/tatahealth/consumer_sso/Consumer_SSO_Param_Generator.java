package com.tatahealth.consumer_sso;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class Consumer_SSO_Param_Generator {

	public static List<NameValuePair> param = null;
	
	// Parameters for login
	public static List<NameValuePair> getLoginParameters(String userName,String password,String platform,String deviceId,String deviceToken) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userLoginName", userName));
		param.add(new BasicNameValuePair("password", password));
		param.add(new BasicNameValuePair("devicePlatform", platform));
		param.add(new BasicNameValuePair("deviceId", deviceId));
		param.add(new BasicNameValuePair("deviceToken", deviceToken));
		//param.add(new BasicNameValuePair("devicePlatform", loginType));
		return param;

	}
	
	// Parameters for logout
	public static List<NameValuePair> getLogoutParameters(int consumerUserId,String deviceId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("deviceId", deviceId));
		return param;

	}
	
	// Parameter for reset password
	public static List<NameValuePair> getResetPasswordParameters(String userName,String password) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("userName", userName));
		param.add(new BasicNameValuePair("password", password));
		return param;

	}
	
	// Parameter for change password
	public static List<NameValuePair> getChangePasswordParameters(int consumerUserId,String newPassword, String oldPassword) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("newPassword", newPassword));
		param.add(new BasicNameValuePair("oldPassword", oldPassword));
		return param;

	}
	
	// Parameter for forget username
	public static List<NameValuePair> getforgotUsernameParameters(String mobileNo,String platform) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("mobileNo", mobileNo));
		param.add(new BasicNameValuePair("platform", platform));
		return param;

	}
	
	// Parameter for verify username
	public static List<NameValuePair> getVerifyLoginNameParameters(String loginName) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("loginName", loginName));
		return param;

	}
	// Parameter for addEmergencyContactDetail
	public static List<NameValuePair> getaddEmergencyContactDetailParameters(int consumerUserId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		return param;

	}
	
	public static JSONObject getJSONgetaddEmergencyContactDetailParameters(String contactId, String contactNo,String firstName, String imagePath, String lastName, int relationshipTypeId ) 
	{
		
		JSONObject getaddEmergencyContactDetailParameters= new JSONObject();
		
		JSONArray contactDetails= new JSONArray();
		JSONObject contactDetailsJsonObject= new JSONObject();
		JSONArray contactList= new JSONArray();
		JSONObject contactListJsonObject= new JSONObject();
		contactListJsonObject.put("contactId", contactId);
		contactListJsonObject.put("contactNo", contactNo);
		contactList.put(contactListJsonObject);
		contactDetailsJsonObject.put("contactList", contactList);
		
		contactDetailsJsonObject.put("firstName", firstName);
		contactDetailsJsonObject.put("imagePath", imagePath);
		contactDetailsJsonObject.put("lastName", lastName);
		contactDetailsJsonObject.put("relationshipTypeId", String.valueOf(relationshipTypeId));
		
		contactDetails.put(contactDetailsJsonObject);
		
		
		getaddEmergencyContactDetailParameters.put("contactDetails",contactDetails);
		return getaddEmergencyContactDetailParameters;
		
		
		
		

	}
	
	
	
	// Parameter for getConsumerAccountDetail
	public static List<NameValuePair> getgetConsumerAccountDetailParameters(int consumerUserId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		return param;

	}
	
	// Parameter for getEmergencyContactDetails
	public static List<NameValuePair> getEmergencyContactDetailsParameters(int consumerUserId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		return param;

	}
	
	// Parameter for removeEmergencyContactDetails
	public static List<NameValuePair> getremoveEmergencyContactDetails(int consumerUserId, int contactDetailsId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("contactDetailsId", String.valueOf(contactDetailsId)));
		return param;

	}
	
	// Parameter for updateEmergencyContactDetail
	public static List<NameValuePair> getupdateEmergencyContactDetailParameters(int consumerUserId,int contactDetailsId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("contactDetailId", String.valueOf(contactDetailsId)));
		return param;

	}
	
	public static JSONObject getJSONupdateEmergencyContactDetailParameters(int contactId, String contactNo,String firstName, String imagePath, String lastName, int relationshipTypeId ) 
	{
		
		JSONObject getaddEmergencyContactDetailParameters= new JSONObject();
		
		JSONArray contactList= new JSONArray();
		JSONObject contactListJsonObject= new JSONObject();
		contactListJsonObject.put("contactId", contactId);
		contactListJsonObject.put("contactNo", contactNo);
		contactList.put(contactListJsonObject);
		
		getaddEmergencyContactDetailParameters.put("firstName", firstName);
		getaddEmergencyContactDetailParameters.put("imagePath", imagePath);
		getaddEmergencyContactDetailParameters.put("lastName", lastName);
		getaddEmergencyContactDetailParameters.put("relationshipTypeId", relationshipTypeId);
		
		
		
		
		getaddEmergencyContactDetailParameters.put("contactList",contactList);
		return getaddEmergencyContactDetailParameters;
		
		
		
		

	}
	
	
	// Parameters for addOrUpdateWorkDetail
	public static List<NameValuePair> getaddOrUpdateWorkDetailParameters(String city,String companyAddress,String companyName,int consumerUserId,String consumerWorkingCompanyId, String phoneNo, String state) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("city", city));
		param.add(new BasicNameValuePair("companyAddress", companyAddress));
		param.add(new BasicNameValuePair("companyName", companyName));
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("consumerWorkingCompanyId", consumerWorkingCompanyId));
		param.add(new BasicNameValuePair("phoneNo", phoneNo));
		param.add(new BasicNameValuePair("state", state));
		return param;

	}
	
	// Parameter for getWorkDetail
	public static List<NameValuePair> getgetWorkDetailParameters(int consumerUserId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		return param;

	}
	
	// Parameters for saveOrUpdateConsumerAddress
	public static List<NameValuePair> getsaveOrUpdateConsumerAddressParameters(String address,String addressType,String city,int consumerUserId,String companyPhone, String companyName, String consumerAddressId, String landmark, String pincode, String state) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("address", address));
		param.add(new BasicNameValuePair("addressType", addressType));
		param.add(new BasicNameValuePair("city", city));
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("companyPhone", companyPhone));
		param.add(new BasicNameValuePair("companyName", companyName));
		param.add(new BasicNameValuePair("consumerAddressId",String.valueOf(consumerAddressId)));
		param.add(new BasicNameValuePair("landmark", landmark));
		param.add(new BasicNameValuePair("pincode", pincode));
		param.add(new BasicNameValuePair("state", state));
		return param;

	}
		
	
	// Parameter for getConsumerAddress
	public static List<NameValuePair> getgetConsumerAddressParameters(int consumerUserId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		return param;

	}
	
	
	// Parameter for setCurrentAddress
	public static List<NameValuePair> getsetCurrentAddressParameters(int consumerUserId, int consumerAddressId, String isCurrentAddress) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("consumerAddressId", String.valueOf(consumerAddressId)));
		param.add(new BasicNameValuePair("isCurrentAddress", isCurrentAddress));
		return param;

	}
	
	// Parameter for removeConsumerAddress
	public static List<NameValuePair> getremoveConsumerAddressParameters(int consumerUserId, int consumerAddressId) 
	{

		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("consumerUserId", String.valueOf(consumerUserId)));
		param.add(new BasicNameValuePair("consumerAddressId", String.valueOf(consumerAddressId)));
		return param;

	}
		

	
}