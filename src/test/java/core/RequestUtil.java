package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Reporter;
import org.xml.sax.SAXException;

public class RequestUtil {
	private final String HEADER_USER_AGENT = "UserAgent";
	private final String HEADER_CONTENT_TYPE = "Content-Type";
	private final String HEADER_CONTENT_LENGTH = "Content-Length";
	private final String HEADER_MACHINE_IDENTIFIER = "userMachineIdentifier";
	private final String HEADER_REQ_DATE = "Request-Date";
	public HttpClient httpClient;

	public HttpResponse getRequest(String restURL) throws ClientProtocolException, IOException {
		httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(restURL);
		Reporter.log("API: "+restURL);
		HttpResponse httpResponse = httpClient.execute(request);
		return httpResponse;
	}
	
	
	
	public HttpResponse getRequestWithParams(String restURL) throws ClientProtocolException, IOException, URISyntaxException {
		httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(restURL);
		
		URI uri = new URIBuilder(request.getURI()).addParameter("consumerId","12").build();
		
		((HttpRequestBase) request).setURI(uri);
		
		Reporter.log("API: "+restURL);
		HttpResponse httpResponse = httpClient.execute(request);
		return httpResponse;
	}

	public HttpResponse getRequest_With_Json_ContentType(String restURL) throws ClientProtocolException, IOException {
		//httpClient = HttpClients.createDefault();
		Reporter.log(restURL);
		HttpGet request = new HttpGet(restURL);
		request.setHeader("Content-Type", "application/json");
		HttpResponse httpResponse = httpClient.execute(request);
		return httpResponse;
	}

	/**
	 * get JSON and with Content Type
	 * 
	 * @param restURL
	 * @param csrfRequestIdentifier
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse getRequest_With_Json_ContentType(String restURL, String csrfRequestIdentifier) throws ClientProtocolException, IOException {
		 httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(restURL);
		request.setHeader("Content-Type", "application/json");
		request.setHeader("csrfRequestIdentifier", csrfRequestIdentifier);
		Header[] requestHeaders = request.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		System.out.println("Hitting Get request:" + request);
		HttpResponse httpResponse = httpClient.execute(request);
		System.out.println("Response Header for Post:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}
		return httpResponse;
	}

	/**
	 * Get Request with JSON
	 * 
	 * @param restURL
	 * @param token
	 * @param csrfRequestIdentifier
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse getRequest_With_Json_ContentType(String restURL, String token, String csrfRequestIdentifier) throws ClientProtocolException, IOException {
		httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(restURL);
		request.setHeader(HEADER_USER_AGENT, "Chrome");
		request.setHeader(HEADER_CONTENT_TYPE, "application/json");
		request.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
		request.setHeader("Accept", "application/json");
		request.setHeader("token", token);
		
		Header[] requestHeaders = request.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		System.out.println("Hitting Get request:" + request);
		HttpResponse httpResponse = httpClient.execute(request);
		System.out.println("Response Header for Post:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}
		return httpResponse;
	}

	
	public HttpResponse postRequest(String restURL, String csrfRequestIdentifier_Header, List<NameValuePair> params) throws ClientProtocolException, IOException {
		System.out.println("--------------------------------------------");

		HttpPost httpPost = new HttpPost(restURL);
		
		httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded");
		if (csrfRequestIdentifier_Header != null) {
			System.out.println("Adding new header");
			httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println(httpResponse.getStatusLine().getStatusCode());

		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		// String responseString = EntityUtils.toString(respEntity, "UTF-8");
		// System.out.println("Response Data/Body:");
		// System.out.println("Response:" + responseString);

		/*
		 * if (respEntity != null) { // EntityUtils to get the response content
		 * String content = EntityUtils.toString(respEntity);
		 * //System.out.println("Content:"+content); }
		 */

		return httpResponse;
	}

	/**
	 * Post JSON request method is used to post the Request in the JSON format.
	 * 
	 * @param restURL
	 * @param csrfRequestIdentifier_Header
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public HttpResponse postJSON_Request(String restURL, String csrfRequestIdentifier_Header, List<NameValuePair> params) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");

		httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
		httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {

			}
		}
		System.out.println("Request Message:" + JSONObjectData.toString());
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println(httpResponse.getStatusLine().getStatusCode());

		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}

	/**
	 * Post JSON request method is used to post the Request in the JSON format.
	 * 
	 * @param restURL
	 * @param token
	 * @param csrfRequestIdentifier_Header
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public HttpResponse postJSON_Request(String restURL,  List<NameValuePair> params) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");
System.out.println( "request url  "+restURL);
Reporter.log("API: "+restURL+"");
		// httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		//httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
	//	httpPost.setHeader("token", token);
	//	httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//	Date date = new Date();
	//	httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {

			}
		}
		System.out.println("Request Message:" + JSONObjectData.toString());
		String Request= JSONObjectData.toString();
		
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse=null;
		httpClient = HttpClients.createDefault();
			try{
			httpResponse = httpClient.execute(httpPost);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	
			
	
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		Reporter.log("Request Message: "+Request+"");
		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}

	
	/////////////////////////////////////////////////////////
	
	
	public HttpResponse postJSONArrayy_Request(String restURL,  List<NameValuePair> params,JSONObject JSONObjectData) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");
System.out.println( "request url  "+restURL);
Reporter.log("API : "+restURL+"");
		 httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
	//	httpPost.setHeader("token", token);
	//	httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.


		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
				
			} catch (JSONException e) {

			}
		}
		String Request= JSONObjectData.toString();
		
		System.out.println("Request Message:" + JSONObjectData.toString());
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		Reporter.log("Request Message: "+Request+"");
		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}
	//////////////////////////////////////////////
	
	
	public HttpResponse postJSONArrayy_Request(String restURL, String token, List<NameValuePair> params,JSONObject JSONObjectData) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");
System.out.println( "request url  "+restURL);
Reporter.log("API : "+restURL+"");
		 httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("tata_auth_token", token);
	//	httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.


		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
				
			} catch (JSONException e) {

			}
		}
		String Request= JSONObjectData.toString();
		
		System.out.println("Request Message:" + JSONObjectData.toString());
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		Reporter.log("Request Message: "+Request+"");
		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}
	
	
	
	
	
	
	
	
	/**
	 * Post JSON request method is used to post the Request in the JSON format.
	 * 
	 * @param restURL
	 * @param token
	 * @param csrfRequestIdentifier_Header
	 * @param jsonObj
	 * @return
	 * @throws Exception
	 */
	public HttpResponse postJSON_Request(String restURL, String token, String csrfRequestIdentifier_Header, JSONObject jsonObj) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");

		// httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
		httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("token", token);
		httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));

		System.out.println("Request Message:" + jsonObj.toString());
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(jsonObj.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println(httpResponse.getStatusLine().getStatusCode());

		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}

	public HttpResponse postJSONArrayy_Request(String restURL, JSONObject JSONObjectData) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");
System.out.println( "request url  "+restURL);
Reporter.log("API : "+restURL+"");
		// httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
	//	httpPost.setHeader("token", token);
	//	httpPost.setHeader("csrfReque-+stIdentifier", csrfRequestIdentifier_Header);
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date date = new Date();
		//httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.


	/*	for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
				
			} catch (JSONException e) {

			}
		}*/
		String Request= JSONObjectData.toString();
		
		System.out.println("Request Message......    :" + JSONObjectData.toString());
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		Reporter.log("Request Message: "+Request+"");
		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}
	
	
	public HttpResponse postRequest(String restURL, String token, String csrfRequestIdentifier_Header) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");

		// httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
		httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("token", token);
		httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));

		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:"+restURL);
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println(httpResponse.getStatusLine().getStatusCode());

		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}

	public String getMimeType(HttpResponse httpResponse) throws ClientProtocolException, IOException {

		// Assert.assertEquals(expectedMimeType,ContentType.getOrDefault(httpResponse.getEntity()).getMimeType());
		return ContentType.getOrDefault(httpResponse.getEntity()).getMimeType();
	}

	/**
	 * Get the JSON Object from the HTTP Response, JSON Object starts With { and
	 * Ends with }
	 * 
	 * @param httpResponse
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws JSONException
	 */
	public JSONObject getJSONObjectForResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, JSONException {
		HttpEntity respEntity = httpResponse.getEntity();
		String responseString = EntityUtils.toString(respEntity, "UTF-8");
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		//Reporter.log(httpResponse.getStatusLine().getStatusCode());
		Reporter.log("Response: "+responseString+"");
		// Convert the response to a String format
		System.out.println("Response:" + responseString);
		// Convert the result as a String to a JSON object
		JSONObject jo = new JSONObject(responseString);

		return jo;

	}

	/**
	 * Generate the JSON object from the JSOn Array and Return the JSOn Object,
	 * as Json Array Starts With [ and Ends with ]
	 * 
	 * @param httpResponse
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws JSONException
	 */
	public JSONObject getJSONobject_From_JSONArrayForResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, JSONException {
		//HttpEntity respEntity = httpResponse.getEntity();
		//String responseString = EntityUtils.toString(respEntity, "UTF-8");
		JSONObject jsonObject = null;
		BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
		String result ="";
		String output = "";
		while ((result = br.readLine()) != null) {
	
			System.out.println("resultttt  "+result);
		    output = "{"+result;
		    System.out.println("output " +output);
		    
		   // String y = '{'+output.substring(1,4);
		    
		   /* String z =y.replace("O", ":");
		    String v=z.replace("<", "}");*/
		    
		   // String s=v.replace("", "}");
		   // System.out.println("vvvvv "+v);
		    //String z1=v.replace(arg0, arg1)
		  //  String z=y.substring(0, 3)+'}'+y.substring(6,10);
		  //  char x1=output.charAt(0);
		    
		 //  System.out.println(y +"  outtt" );
		 //  StringBuffer x=new StringBuffer(output);
		   
		 
		 
		    
		    
		     jsonObject = new JSONObject(output);     
		}
		System.out.println("Response:" + jsonObject.toString());
		return jsonObject;

	}

	/**
	 * Generates a unique reference id
	 * 
	 * @return
	 */
	public String generateUUID() {
		// Long uuid = UUID.randomUUID().getMostSignificantBits();
		// String uid = String.valueOf(uuid);
		// uid = uid.substring(0, 10);
		DateFormat df = new SimpleDateFormat("ddMMyyHHmmss");
		Date dateobj = new Date();
		String uid = df.format(dateobj);
		// log.info(df.format(dateobj));
		return uid;
	}

	/**
	 * Create a Random No for the length of Given
	 * 
	 * @param len
	 * @return
	 */
	public String createRandomNumber(long len) {
		if (len > 18)
			throw new IllegalStateException("To many digits");
		long tLen = (long) Math.pow(10, len - 1) * 9;

		long number = (long) (Math.random() * tLen) + (long) Math.pow(10, len - 1) * 1;

		String tVal = number + "";
		if (tVal.length() != len) {
			throw new IllegalStateException("The random number '" + tVal + "' is not '" + len + "' digits");
		}
		return tVal;
	}
	
	public String getAccountNumberRandom() {
		int len=9;
		String accountnoStart="50100";
		if (len > 18)
			throw new IllegalStateException("To many digits");
		long tLen = (long) Math.pow(10, len - 1) * 9;

		long number = (long) (Math.random() * tLen) + (long) Math.pow(10, len - 1) * 1;

		String tVal = number + "";
		if (tVal.length() != len) {
			throw new IllegalStateException("The random number '" + tVal + "' is not '" + len + "' digits");
		}
		return accountnoStart+tVal;
	}
	
	
	
	
	public HttpResponse putJSON_Request(String restURL,  List<NameValuePair> params) throws Exception 
	{
		// PropertyConfigurator.configure("./log4j.properties");
			System.out.println( "request url  "+restURL);
			Reporter.log("API: "+restURL+"");
		// httpClient = HttpClients.createDefault();
			HttpPut httpPut = new HttpPut(restURL);
			httpPut.setHeader(HEADER_USER_AGENT, "Chrome");
		//httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
			httpPut.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
	//	httpPost.setHeader("token", token);
	//	httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//	Date date = new Date();
	//	httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {

			}
		}
		System.out.println("Request Message:" + JSONObjectData.toString());
		String Request= JSONObjectData.toString();
		
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPut.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPut.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse=null;
		httpClient = HttpClients.createDefault();
			try{
			httpResponse = httpClient.execute(httpPut);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			System.out.println(httpResponse.getStatusLine());
			System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
			int statusCode= httpResponse.getStatusLine().getStatusCode();
			Reporter.log("STATUS CODE: "+statusCode+"");
			Reporter.log("Request Message: "+Request+"");
			// get all headers
			System.out.println("Response Header:");
			Header[] headers = httpResponse.getAllHeaders();
			for (Header header : headers) {
				System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
			}

			return httpResponse;

		}
	
	
	
	public HttpResponse postJSON_Request(String restURL,  List<NameValuePair> params, String authToken) throws Exception {
		// PropertyConfigurator.configure("./log4j.properties");
		
		System.out.println( "request url  "+restURL);
		Reporter.log("API: "+restURL+"");
		httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(restURL);
		httpPost.setHeader(HEADER_USER_AGENT, "Chrome");
		//httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
		httpPost.setHeader(HEADER_CONTENT_TYPE, "application/json");
	//	httpPost.setHeader(HEADER_MACHINE_IDENTIFIER, "Linux");
	//	httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("tata_auth_token", authToken);
	//	httpPost.setHeader("csrfRequestIdentifier", csrfRequestIdentifier_Header);
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//	Date date = new Date();
	//	httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
		// Create a Json object for the params.
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {

			}
		}
		System.out.println("Request Message:" + JSONObjectData.toString());
		String Request= JSONObjectData.toString();
		
		// Convert the JSON object to String Entity
		StringEntity JsonEntityObj = new StringEntity(JSONObjectData.toString());
		httpPost.setEntity(JsonEntityObj);
		Header[] requestHeaders = httpPost.getAllHeaders();
		System.out.println("Request Header for Post:");
		for (Header header : requestHeaders) {
			System.out.println(header.toString());
		}
		HttpResponse httpResponse=null;
		httpClient = HttpClients.createDefault();
			try{
			httpResponse = httpClient.execute(httpPost);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	
			
	
		// HttpEntity respEntity = httpResponse.getEntity();

		System.out.println(httpResponse.getStatusLine());
		System.out.println("STATUS CODE:" +httpResponse.getStatusLine().getStatusCode());
		int statusCode= httpResponse.getStatusLine().getStatusCode();
		Reporter.log("STATUS CODE: "+statusCode+"");
		Reporter.log("Request Message: "+Request+"");
		// get all headers
		System.out.println("Response Header:");
		Header[] headers = httpResponse.getAllHeaders();
		for (Header header : headers) {
			System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());
		}

		return httpResponse;

	}

	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

