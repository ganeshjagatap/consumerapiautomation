package core;

public class Constants {
	
	public static String CREATE_SESSION_PATH="/rest/session/create";
	
	
	// Consumer SSO
	public static String login= "consumerOnboarding/api/v1/userLogin";
	public static String logout="consumerOnboarding/api/v1/logout";
	public static String resetPassword="consumerOnboarding/api/v1/resetPassword";
	public static String changePassword="consumerOnboarding/api/v1/changePassword";
	public static String forgotUsername="consumerOnboarding/api/v1/forgotUsername";
	public static String verifyLoginName="consumerOnboarding/api/v1/verifyLoginName";
	public static String addEmergencyContactDetail="consumerOnboarding/api/v1/addEmergencyContactDetails";
	public static String getConsumerAccountDetail="consumerOnboarding/api/v1/getConsumerAccountDetail";
	public static String getEmergencyContactDetails="consumerOnboarding/api/v1/getEmergencyContactDetails";
	public static String removeEmergencyContactDetails="consumerOnboarding/api/v1/removeEmergencyContactDetails";
	public static String updateEmergencyContactDetail="consumerOnboarding/api/v1/updateEmergencyContactDetails";
	public static String addOrUpdateWorkDetail="consumerOnboarding/api/v1/saveOrUpdateWorkDetail";
	public static String getWorkDetail="consumerOnboarding/api/v1/getWorkDetail";
	public static String saveOrUpdateConsumerAddress="consumerOnboarding/api/v1/saveOrUpdateConsumerAddress";
	public static String getConsumerAddress="consumerOnboarding/api/v1/getConsumerAddress";
	public static String setCurrentAddress="consumerOnboarding/api/v1/setCurrentAddress";
	public static String removeConsumerAddress="consumerOnboarding/api/v1/removeConsumerAddress";
	
	
}
