package core;

import java.io.FileInputStream;
import java.util.Properties;

public class SetUp {
	private FileInputStream configFile = null;
	public static Properties testConfigFile = null;
	

	//*************************************************************************************************************************
		//Generic function name :SetUp
		//Description :Setting up the desired capabilities
		//Author: Ganesh Jagatap
		//
		//*************************************************************************************************************************	
	
	public SetUp() {
		try {
			configFile = new FileInputStream(".//ConfigFiles//config.properties");
			testConfigFile = new Properties();
			testConfigFile.load(configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
