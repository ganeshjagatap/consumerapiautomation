package core;



public class CommonData {

	private static String authToken;

	public static String getauthToken() {
		return authToken;
	}

	public static void setauthToken(String authToken) {
		CommonData.authToken = authToken;
	}
	
	
	private static int consumerUserId;

	public static int getconsumerUserId() {
		return consumerUserId;
	}

	public static void setconsumerUserId(int consumerUserId) {
		CommonData.consumerUserId = consumerUserId;
	}
	
	private static String userName;

	public static String getuserName() {
		return userName;
	}

	public static void setuserName(String userName) {
		CommonData.userName = userName;
	}
	
	private static String mobileNumber;

	public static String getmobileNumber() {
		return mobileNumber;
	}

	public static void setmobileNumber(String mobileNumber) {
		CommonData.mobileNumber = mobileNumber;
	}
	
	private static int contactDetailId;

	public static int getcontactDetailId() {
		return contactDetailId;
	}

	public static void setcontactDetailId(int contactDetailId) {
		CommonData.contactDetailId = contactDetailId;
	}
	
	
	private static int contactId;

	public static int getcontactId() {
		return contactId;
	}

	public static void setcontactId(int contactId) {
		CommonData.contactId = contactId;
	}
	
	private static int consumerAddressID;

	public static int getconsumerAddressID() {
		return consumerAddressID;
	}

	public static void setconsumerAddressID(int consumerAddressID) {
		CommonData.consumerAddressID = consumerAddressID;
	}
	
}
